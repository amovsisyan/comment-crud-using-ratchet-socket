<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Storage;
use App\User;
use App\Post;
use File;

class PostController extends Controller
{
    public function index(){
        return view('addPost');
    }

    public function addPost(Request $request){
        if($request->isMethod('post')){
            $rules = [
                'postName'=>'required|min:3|max:30',
                'postContent'=>'required|min:3|max:1000',
                'file'=>'image|required',
            ];
            $this->validate($request,$rules);
        }

        $file = $request->file('file');
        $filename = time().".".$file->getClientOriginalExtension();

        /*adding*/
        $created_by = Auth::id();
        $user = User::find($created_by);
        $new_post = new Post([
            'post_name'=>$request->postName,
            'post_content'=>$request->postContent,
            'post_avatar'=>$filename,

        ]);

        $user->posts()->save($new_post);

        Storage::disk('public')->put($filename,File::get($file));

        return redirect()->route('allposts');

    }

    public function allPosts(){
        $per_page = 4;
        $all_posts = Post::paginate($per_page);
        return view('allPosts', ['all_posts' => $all_posts]);
    }

    public function currentPost($id){
        $current_post = Post::where('post_id',$id)->first();
        $comments_for_this = $current_post->comments()->
        leftJoin('users', function ($join) {
            $join->on('comments.created_by', '=', 'users.id');
        })->select('users.name', 'comments.created_at', 'comments.comment_content')->orderBy('comments.created_at','desc')->get();
        $auth =  User::where('id',Auth::id())->select('name','id')->first();
        return view('currentPost', ['current_post' => $current_post,'comments' => $comments_for_this,'auth'=>$auth]);
    }

    public function deletePost(Request $request){
        try{
            $id = $request['id'];
            $img_name = Post::select('post_avatar')->where('post_id', '=', $id)->get();
            $img_path = $img_name[0]->post_avatar;

            File::delete('img/posts/'.$img_path);

            Post::where('post_id', '=', $id)->delete();

            return json_encode(array('post_id'=>$id));
        }
        catch (\Exception $e){
            return json_encode(array('error'=>$e->getMessage()));
        }
    }

    public function changePost(Request $request){
        try {
            if($request->isMethod('post')){
                $rules = [
                    'post_name'=>'required|min:3|max:30',
                    'post_content'=>'required|min:3|max:1000',
                ];
                $this->validate($request,$rules);
            }


            $post_name = $request['post_name'];
            $post_content = $request['post_content'];
            $id = $request['id_post'];

            if($request->file('img')){
                $img_name = Post::select('post_avatar')->where('post_id', '=', $id)->get();
                $img_path = $img_name[0]->post_avatar;

                File::delete('img/posts/'.$img_path);

                $file = $request->file('img');

                $filename = time().".".$file->getClientOriginalExtension();

                Storage::disk('public')->put($filename,File::get($file));
            }

            $user_id = Auth::id();
            $user = User::find($user_id);

            $user->posts()->where('post_id',$id)->update([
                'post_name'=>$post_name,
                'post_content'=>$post_content,
            ]);
            if (isset($filename)){
                $user->posts()->where('post_id',$id)->update([
                    'post_avatar'=>$filename,
                ]);
                return json_encode(array('post_id'=>$id,'post_name'=>$post_name,'post_content'=>$post_content,'post_avatar'=>$filename));
            }

            return json_encode(array('post_id'=>$id,'post_name'=>$post_name,'post_content'=>$post_content));
        }
        catch (\Exception $e) {
            return json_encode(array('error'=>$e->getMessage()));
        }

    }
}
