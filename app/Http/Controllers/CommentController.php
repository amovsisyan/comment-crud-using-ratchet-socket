<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use Illuminate\Http\Request;
use Auth;

class CommentController extends Controller
{
    public function addComment(Request $request,$id){
        if($request->comment){
            try{
                $auth = Auth::id();
                $post_id = $id;

                $id = Comment::insertGetId(
                    [
                        'created_by'=>$auth,
                        'comment_content'=>$request->comment,
                        'for_post' => $post_id,
                    ]
                );
                $comment_append = Comment::where('comment_id',$id)->
                leftJoin('users', function ($join) {
                    $join->on('comments.created_by', '=', 'users.id');
                })->select('users.name', 'comments.created_at', 'comments.comment_content')->orderBy('comments.created_at','asc')->get();
                return json_encode($comment_append);
            } catch(\Exception $e){
                return json_encode(array('error'=>$e->getMessage()));
            }

        } else {
          return 0;
        }
    }

}
