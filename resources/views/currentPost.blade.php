@extends('layouts.app')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">


                    <div class="col-md-12 getter" id ="{{$current_post->post_id}}">
                        <div class="outline">
                            @if(Auth::id()== $current_post->created_by)
                                <div><button class="btn btn-danger btn-sm post_but post_del">X</button></div>
                                <div><button class="btn btn-warning btn-sm post_but post_edit">ed</button></div>
                            @endif
                            <div class="col-xs-4">
                                <img src="/img/posts/{{ $current_post->post_avatar }}" alt="">
                            </div>
                            <div class="col-xs-8">
                                <div class="info">
                                    <p id="post_name">{{$current_post->post_name}}</p>
                                    <p id="post_content">{{$current_post->post_content}}</p>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>

        </div>

    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12" id = "writingArea">
                <div class="form-group">
                    <label for="comment">Comment:</label>
                    <textarea class="form-control comment-control" rows="3" id="comment"></textarea>
                    <div class="btn btn-success" id = "btn_send_comment">Commit</div>
                    <input type="hidden" name="_id" id="auth_id" value="{{$auth->id}}">
                    <input type="hidden" name="_name" id="auth_name" value="{{$auth->name}}">
                </div>
            </div>

        </div>

    </div>


    <div class="container">
        <div class="row needed">
            @foreach ($comments as $comment)
                <div class="col-xs-12">
                    <div class="panel panel-white post panel-shadow">
                        <div class="post-heading">
                            <div class="pull-left meta">
                                <div class="title h5">
                                    <a href="#"><b>{{ $comment->name }}</b></a>
                                </div>
                                <h6 class="text-muted time">made a post on. {{ $comment->created_at }}</h6>
                            </div>
                        </div>
                        <div class="post-description">
                            <p>{{ $comment->comment_content }}</p>
                        </div>
                    </div>
                </div>
                @endforeach
        </div>
    </div>





    <script>
        $('#comment').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                $("#btn_send_comment").click();
                e.preventDefault()
            }});

//        $(document).on("click","#btn_send_comment",function() {
//            var comment = $('#comment').val();
//            if(!comment) return;
//            $.ajax({
//                type:'put',
//                url:location.pathname,
//                dataType: "json",
//                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
//                data:{
//                    comment:comment
//                },
//                success:function(data){
//                    if(!data || data.error) return;
//                    console.log(data)
//                    $('.needed').prepend('<div class="col-xs-12"> ' +
//                        '<div class="panel panel-white post panel-shadow"> ' +
//                        '<div class="post-heading"> <div class="pull-left meta"> ' +
//                        '<div class="title h5"> <a href="#"><b>' + data[0]['name'] +'</b>' +
//                    '</a> </div>  <h6 class="text-muted time">made a post on. '+ data[0]['created_at'] +'</h6> ' +
//                        '</div>  </div> <div class="post-description">' +
//                        ' <p>'+ data[0]['comment_content'] +'</p>          </div>              </div>          </div>')
//                    $('#comment').val('');
//                }
//            })
//        });




        var conn  = new WebSocket ('ws://artcode.com:8080');

        conn.onopen = function (e){
            console.log('Connection Opened')
        };

        conn.onmessage = function (e) {
            var entity = JSON.parse(e.data);
            console.log('entity ====', entity.isTyping);
//            console.log('Message888888888888888' + e.data)
            if (entity.location == window.location.pathname){
                if(entity.newComm){
                    var data = entity.createdAt.date;
                    var resData = data.slice(0, data.length-7);
                    $('.needed').prepend('<div class="col-xs-12"> ' +
                        '<div class="panel panel-white post panel-shadow"> ' +
                        '<div class="post-heading"> <div class="pull-left meta"> ' +
                        '<div class="title h5"> <a href="#"><b>' + entity.name +'</b>' +
                        '</a> </div>  <h6 class="text-muted time">made a post on. '+ resData +'</h6> ' +
                        '</div>  </div> <div class="post-description">' +
                        ' <p>'+ entity.commentContent +'</p>          </div>              </div>          </div>')
                    $('#comment').val('');
                }

                if(entity.isTyping){
                    var id = entity.auth_id;
                    if ($('#t_'+id).length != 0) return;
                    $('#writingArea').find('.form-group').prepend('<div id=t_'+ id +' class="floatRight">'+ entity.auth_name + ' is typing   </div>');
                    setTimeout(function() {
                        $('#t_'+id).remove();
                    }, 5000);
                }




            }
        };

        $(document).on("click","#btn_send_comment",function() {
            var comment = $('#comment').val();
            if(!comment) return;
            var location = window.location.pathname;
            var res = location.split("/");
            var auth_id = $('#auth_id').val();
            var auth_name = $('#auth_name').val();
            var data = {};
            data.auth_id = auth_id;
            data.comment = comment;
            data.comment_id = res[2];
            data.location = location;
            data.newComm = true;

            var dateObj = new Date();
            var year = dateObj.getFullYear();
            var month = dateObj.getMonth() + 1;
            var day = dateObj.getDate();
            var hour = dateObj.getHours();
            var min = dateObj.getMinutes();
            var sec = dateObj.getSeconds();

            var newdate = year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;

            var sending = JSON.stringify(data);
            conn.send(sending);
            $('.needed').prepend('<div class="col-xs-12"> ' +
                '<div class="panel panel-white post panel-shadow"> ' +
                '<div class="post-heading"> <div class="pull-left meta"> ' +
                '<div class="title h5"> <a href="#"><b>' + auth_name +'</b>' +
                '</a> </div>  <h6 class="text-muted time">made a post on. '+ newdate +'</h6> ' +
                '</div>  </div> <div class="post-description">' +
                ' <p>'+ comment +'</p>          </div>              </div>          </div>')
            $('#comment').val('');
        });

        $('#comment').bind('keypress', function (e) {
            if (e.keyCode == 32) {
                var comment = $('#comment').val();
                if(!comment) return;

                var auth_name = $('#auth_name').val();
                var auth_id = $('#auth_id').val();
                var data = {};
                var location = window.location.pathname;
                data.auth_name = auth_name;
                data.location = location;
                data.isTyping = true;
                data.auth_id = auth_id;

                var sending = JSON.stringify(data);
                conn.send(sending);
            }});
    </script>
@endsection
