@extends('layouts.app')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                @foreach($all_posts as $post)

                <div class="col-md-12 getter" id ="{{$post->post_id}}">
                    <div class="outline">
                        @if(Auth::id()== $post->created_by)
                            <div><button class="btn btn-danger btn-sm post_but post_del">X</button></div>
                            <div><button class="btn btn-warning btn-sm post_but post_edit">ed</button></div>
                        @endif
                       <div class="col-xs-4">
                           <img src="/img/posts/{{ $post->post_avatar }}" alt="">
                       </div>
                        <div class="col-xs-8">
                            <div class="info">
                                <p id="post_name">{{$post->post_name}}</p>
                                <p id="post_content">{{$post->post_content}}</p>
                            </div>
                        </div>
                        <div class="see_more">
                            <a href="/allposts/{{ $post->post_id }}">
                                See More ->
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            {{ $all_posts->links() }}
        </div>

    </div>

    <div class="col-md-8 col-md-offset-2 pop_up">

            <div class="form-group">
                <label>Your Post Name</label>
                <input type="text" class="form-control" id="postName" placeholder="Post Name" name="postName">
            </div>
            <div class="form-group">
                <label>Your Post Content</label>
                <input type="text" class="form-control" id="postContent" placeholder="Post Content" name="postContent">
            </div>
            <div class="form-group">
                <label>Post IMG</label>
                <input type="file" class="form-control" id="file" name="file">
            </div>

        <div class="form-group">
            <label for="exampleInputPassword1"></label>
            <input id="hidden_type" type="hidden" value="">
        </div>

            <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <button id="save_change" class="btn btn-success">Save Changes</button>
        <button id="cancel" class="btn btn-default">Cancel</button>
    </div>

<script>
    $(document).ready(function () {
        $(document).on("click",".post_edit",function() {
            var id = $(this).parents('.getter').attr('id');
            $post_name = $('#'+id).find("#post_name").html();
            $post_content = $('#'+id).find("#post_content").html();
            $('#postName').val($post_name);
            $('#postContent').val($post_content);
            $('#hidden_type').val(id);
            $('.pop_up').fadeIn();
        });

        $(document).on("click","#cancel",function() {
            $('.pop_up').fadeOut();
        });

        $(document).on("click","#save_change",function() {

            var post_name = $('#postName').val();
            var post_content = $('#postContent').val();
            var id_post = $('#hidden_type').val();
            var file2 = document.getElementById("file").files[0];

            var formData = new FormData();

            formData.append('post_name',post_name);
            formData.append('post_content',post_content);
            formData.append('id_post',id_post);
            formData.append('img',file2);
            $.ajax({
                type:'post',
                url:'/changepost',
                cache: false,
                enctype: 'multipart/form-data',
                data:formData,
                dataType: "json",
                processData: false,
                contentType: false,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },

                success:function(data){
                    if(data.error){
                        $('.pop_up .validation_error').remove();
                        $('.pop_up').append('<p class="validation_error"> The input is not valid </p>')
                        return;
                    }
                    var id = data['post_id'];
                    $('#'+id).find('#post_name').html(data['post_name']);
                    $('#'+id).find('#post_content').html(data['post_content']);
                    if(data['post_avatar']){
                        $('#'+data['post_id']).find('img').fadeOut('slow', function(){
                            var thisOne = $('#'+data['post_id']).find('img');
                            thisOne.attr('src','/img/posts/'+data['post_avatar'])
                            thisOne.fadeIn('slow');
                        })
                    }

                    $('.pop_up').fadeOut();

                }
            })
        });

        $(document).on("click",".post_del",function() {
            var id = $(this).parents('.getter').attr('id');
            $.ajax({
                type:'delete',
                url:'/deletepost',
                dataType: "json",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data:{
                    id:id
                },
                success:function(data){
                    if(data.error){
                        alert('The input is not valid'); /* Didn't want waste a time for this*/
                        return;
                    }
                    var dataId = data['post_id'];
                    $('#'+dataId).fadeOut('slow', function(){
                        $('#'+dataId).remove();
                    })
                }
            })
        });


    });
</script>
@endsection
