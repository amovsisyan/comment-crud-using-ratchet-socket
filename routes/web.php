<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();



Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index');

    Route::get('/addpost', 'PostController@index');
    Route::post('/addpost', 'PostController@addPost');

    Route::get('/allposts', ['as'=>'allposts','uses'=>'PostController@allPosts']);
    Route::get('/allposts/{id}', 'PostController@currentPost')->where('id','^[1-9][0-9]*$');
    Route::put('/allposts/{id}', 'CommentController@addComment')->where('id','^[1-9][0-9]*$');

    Route::post('/changepost', 'PostController@changePost');
    Route::delete('/deletepost', 'PostController@deletePost');

});
